var path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'ripser.js',
        library: 'Ripser',
        libraryTarget: 'umd'
    },
    externals: {
        d3: "d3"
    },
    module: {
        rules: [
            {
                test: /\.worker\.js$/,
                use: {
                    loader: 'worker-loader',
                    options: {inline: true,fallback:false/*, fallback: false,publicPath: '../dist/'*/}
                }
            },
            {
                test: /\.wasm$/,
                use: 'arraybuffer-loader'
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {from:"src/index.html"}
            ]),
        new UglifyJsPlugin()
    ],
    node: {
        fs: 'empty'
    }
}
