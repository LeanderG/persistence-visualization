# JavaScript Library for [Ripser](https://github.com/Ripser/ripser)

Ripserjs offers an easy to use API for computing and visualizing Vietoris-Rips persistence barcodes.

## Compatibility

Ripserjs uses WebAssembly and only works on browsers that support WebAssembly.

Tested and working browser are: Chrome, Firefox and Edge 41

Not working browser(s): IE

## Usage

Install using NPM:
```
npm install --save d3 ripser
```

Import using script tags (import d3 first):
```
<script src="/node_modules/d3/build/d3.min.js"></script>
<script src="/node_modules/ripser/dist/ripser.js"></script>
```
Or Import using ES6 Syntax:
```
import * as d3 from "d3";
import * as Ripser from "ripser";
```
### Examples

http://leanderg.gitlab.io/ripserjs_examples/

## API

TODO

```javascript
Ripser.run(data, selector[, options]);
```
#### `data`

The data argument can be either a string or a 2D-Array. Supported formats are lower-distance matrix, upper-distance matrix, distance matrix and point-cloud. The string will be passed to Ripser as is.

#### `selector`

The selector in which the diagram(s) will be put. Most of the time you want to use an id that references a div. Example: `"#div1"`

It can also be an array of selectors like `["#header","#dim0","#dim1","#dim2"]`. Each Diagram will be put in a different div. Length of the array needs to be maxDim - minDim + 1

###### Example:
HTML:
```html
<div id="diagram"></div>
```
JS:
```javascript
Ripser.run(data,"#diagram");
```
#### `options`
Optional.

| Name        | Type     | Value(s)                                                               | Description                             |
|-------------|----------|------------------------------------------------------------------------|-----------------------------------------|
| outputType  | String   | "PersistenceBarCodes" [Default], "PersistenceDiagram", "CSVExporter"   | Type of the output.                     |
| format      | String   | "lower-distance" [Default], "upper-distance", "distance", "point-cloud"| Format of the data argument             |
| minDim      | Integer  | greater or equal to 0 [Default]                                        | Minimum dimension to be displayed       |
| maxDim      | Integer  | greater or equal to minDim, 2 [Default]                                | Maximum dimension to be displayed       |
| threshold   | Float    | Infinity [Default] or greater than 0                                   | Compute Rips complexes up to diameter t |
| callback    | Function |                                                                        | Gets called after computation is done   |


## How to compile

1. (Optional) Compile Ripser from https://github.com/Ripser/ripser/tree/live-webassembly using Emscripten and the following command:
```
Emscripten with emcc -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -Wall --bind --memory-init-file 0 -std=c++11 -o emscripten/ripser.js -O3 -D NDEBUG ripser.cpp
```

Replace src/emscripten/ripser.wasm with the newly compiled file.

In the file ripser.js remove `var Module;` from the beginning and copy the content into the loadWASM function in src/ripser.worker.js. TODO: Find a solution that doesn't require modifying ripser.js

2. Run `npm install` (requires the Node Package Manager)
3. Run `webpack` to compile

## Known Issues
* If Ripser runs out of memory it crashes with nothing but a console error.
* File size is a bit large with 736 KB (not gzipped) and 230 KB (gzipped).
