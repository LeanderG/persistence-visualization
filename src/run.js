import {PersistenceBarChart} from "./BarChart";
import {PersistenceDiagram} from "./Diagram";
import RipserWorker from './ripser.worker.js';
import {CSVExporter} from "./CSVExporter";
export function run(data, domElement, options) {

    var scope = this;
    // private
    var defaultOptions = {
        outputType: "PersistenceBarCodes",
        format: "lower-distance",
        minDim: 0,
        maxDim: 2,
        threshold: Infinity,
        callback: function () {

        }
    };
    var formatMap =
        {
            "lower-distance": 0,
            "upper-distance": 1,
            "distance": 2,
            "point-cloud": 3,
            "dipha": 4
        };
    var dim;
    var _painter;
    var _data;
    var workerRunning = false;
    var timeoutAfter = 20000; // ms

    this.result = {};

    init();
     //var _worker = new RipserWorker();
    if(!window.Worker) {
        throw new Error("Browser does not support web workers.");
    }
    //var _worker = new Worker(workerPath);
    var _worker = new RipserWorker();
    workerRunning = true;
    setTimeout(function() {
        if(workerRunning) {
            console.error("Ripser took too long. Terminating...");
            _worker.terminate();
            var workerRunning = false;
        }
    }, timeoutAfter)

     _worker.addEventListener("message", handleMessage, false);
    _worker.postMessage({type:"loadWasm"});


    function init() {
        options = [defaultOptions, options].reduce(Object.assign, {});
        setPainter(options.outputType);
        setData(data);
    }

    function setPainter(outputType) {
        if (outputType === "PersistenceBarCodes") {
            _painter = new PersistenceBarChart();
        } else if (outputType === "PersistenceDiagram") {
            _painter = new PersistenceDiagram();
        } else if(outputType === "CSVExporter") {
            _painter = new CSVExporter();
        } else if (typeof outputType === "string") {
            throw new Error("Argument " + outputType + " in the option 'outputType' is not legal");
        } else {
            _painter = outputType;
        }
        if (Array.isArray(domElement)) {
            if (domElement.length < options.maxDim - options.minDim + 1) {
                throw new Error("Second argument has to be a selector or an array of selectors with length maxDim - minDim + 1");
            }
            _painter.domElement = domElement[0];
        } else {
            _painter.domElement = domElement;
        }

    }

    function handleMessage(message) {
        if (message.data === undefined) {
            return;
        }
        var messageType = message.data.type;
        if (messageType === "initialized") {
            _worker.postMessage({
                "type": "runRipser",
                "file": _data,
                "dim": options.maxDim,
                "threshold": options.threshold,
                "format": formatMap[options.format]
            });
        }
        else if (messageType === "finished") {
            _worker.terminate();
            workerRunning = false;
            _painter.draw(dim,this.valueRange,scope.result[dim])
            if(_painter.finish) _painter.finish();
            options.callback();
        }
        else if (messageType === "dim" && message.data.dim >= options.minDim) {
            if(scope.result[dim] !== undefined) {
                _painter.draw(dim,this.valueRange,scope.result[dim])
            }
            dim = message.data.dim;
            scope.result[dim] = [];
            if (Array.isArray(domElement)) {
                _painter.domElement = domElement[message.data.dim - options.minDim + 1];
            }
            //_painter.initDiagram(this.valueRange, message.data.dim);

        } else if (messageType === "interval" && message.data.dim >= options.minDim) {

            var isAlive = false;
            if (message.data.death === undefined) {
                message.data.death = this.valueRange[1];
                isAlive = true;
            }

            var dataPoint = {
                "birth": message.data.birth,
                "death": message.data.death,
                "alive": isAlive // is the class still alive after the final filtration step?
            };
            scope.result[message.data.dim].push(dataPoint);
            //_painter.insertDataPoint(dataPoint)

        } else if (messageType === "distance-matrix") {
            this.valueRange = [0, Math.min(message.data.max,options.threshold)];
            this.distancesRange = [message.data.min, message.data.max];
            _painter.valueRange = this.valueRange;
            _painter.distancesRange = this.distancesRange;
            if (_painter.headingRange == null) {
                console.debug("Function headingRange(range) not defined on painter. The diagram will not output the range of distances from the distance matrix.");
                return;
            }
            if (_painter.outputRange == null) {
                console.debug("Function outputRange not defined on painter. The diagram will not output the range of distances from the distance matrix.")
                return;
            }
            _painter.outputRange();

        }
    }

    this.stop = function (deleteDomContent) {
        deleteDomContent = deleteDomContent || false;
        _worker.terminate();
        if(deleteDomContent) {
            if(Array.isArray(domElement)) {
                domElement.forEach(function (element) {
                    document.querySelector(element).innerHTML = '';
                })
            } else {
                document.querySelector(domElement).innerHTML = '';
            }
        }
    }

    function setData(data) {
        if (typeof data === "string") {
            _data = data;
        } else if (Array.isArray(data)) {
            _data = matrixToString(data);
        } else {
            throw new Error("Input data incorrectly formatted. Use a String or 2D-Array");
        }
    }

    function matrixToString(matrix) {
        return matrix.join("\r\n");
    }
}