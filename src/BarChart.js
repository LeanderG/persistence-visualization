import * as d3 from "d3";
import {getKey,chop,formatRange} from "./Util";

export function PersistenceBarChart() {


    // private
    var scope = this;
    var _data = [];
    var _index = [];

    var _x;
    var _y;
    var _svg;
    var _g;
    var _dimension;

    // public
    this.range = [];
    this.domElement;
    this.colorScheme = d3.schemeCategory10;
    this.barOutterHeight = 12;
    this.barInnerHeight = 4;
    this.margin = {top: 40, right: 20, bottom: 20, left: 20};
    this.width = 350;
    this.height = 350;

    this.headingPerDimension = function (dimension) {
        return '<h4>Persistence intervals in dimension ' + dimension + '</h4>';
    };

    this.headingRange = function (range) {
        var formattedRange = formatRange(range);
        return '<h4>Distances have the range [ ' + formattedRange[0] + ", " + formattedRange[1] + ' ]</h4>'
    };

    this.outputRange = function () {
        d3.select(this.domElement).append("div").html(this.headingRange(this.distancesRange));
    };

    this.draw = function(dimension, valueRange, data) {
        var barHeight = 20;
        _x = d3.scaleLinear().domain(valueRange).range([0, this.width - this.margin.left - this.margin.right]);

        //insert heading
        d3.select(this.domElement).append("div").html(this.headingPerDimension(dimension));

        _svg = d3.select(this.domElement).append("svg")
            .attr("width", this.width)
            .attr("height", this.margin.top + this.margin.bottom + data.length*this.barOutterHeight);

        _svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
            .call(d3.axisTop().scale(_x));

        _g = _svg.append("g")
            .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + 10) + ")")

        _index.push(_data.length - 1);
        data.sort(function (a, b) {
            return (b.death - b.birth - a.death + a.birth) || (a - b);
        });

        var bar = _g.selectAll("g")
            .data(data)
            .enter().append("g")
            .attr("transform", function(d, i) { return "translate(0," + (i * scope.barOutterHeight) + ")"; });

        bar.append("rect")
            .attr("x",function(d) {return _x(d.birth)})
            .attr("width", function(d) { return _x(d.death-d.birth); })
            .attr("height", this.barInnerHeight)
            .attr("fill", this.colorScheme[dimension % 10])
            .append("title").html(function (d) {
            return "[" + chop(d.birth).toString() + ",&thinsp;" + chop(d.death).toString() + ")";
        });

        // add triangles to bar codes that are still alive after final filtration
        bar.filter(function (d) {
            return d.alive
        })
            .append("path")
            .attr("d","M -1 "+ -(1*this.barInnerHeight/2+1) + " L " + (this.barInnerHeight) + " " + this.barInnerHeight/2 +" L -1 " + (3*this.barInnerHeight/2+1) +" z")
            .attr("fill",this.colorScheme[dimension % 10])
            .attr("transform", function(d) {return "translate(" + _x(d.death) + "," + 0 + ")"});

    }

}