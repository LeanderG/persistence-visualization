export function CSVExporter() {
    var rows = [];
    var currentDimension;
    // this.initDiagram = function (newValueRange, newDimension) {
    //     currentDimension = newDimension;
    // }
    //
    // this.insertDataPoint = function (dataPoint) {
    //     dataPoint.dim = currentDimension;
    //     rows.push(dataPoint);
    // }

    this.draw = function(dimension, valueRange, data) {

        data.forEach(function(dataPoint) {
            dataPoint.dim = dimension;
            rows.push(dataPoint);
        })
    }
    this.finish = function () {
        console.log(rows);
        var csvContent = "data:text/csv;charset=utf-8,";
        csvContent += "dimension, birth, death, alive after filtration\r\n"
        rows.forEach(function(dataPoint){
            var row = [dataPoint.dim,dataPoint.birth,dataPoint.death,dataPoint.alive].join(", ");
            csvContent += row + "\r\n"; // add carriage return
        });
        var data = encodeURI(csvContent);
        var link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', "result_" + new Date().toLocaleString()+".csv");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}