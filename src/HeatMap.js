import * as d3 from "d3";
import * as d3ScaleChromatic from 'd3-scale-chromatic';
export function PersistenceHeatMap() {
    var scope = this;
    // public
    this.colorScheme = d3ScaleChromatic.interpolateYlGnBu;

    this.margin = {top: 40, right: 20, bottom: 40, left: 60};
    this.width = 300;
    this.height = 300;
    this.resolution = 30;
    var maxValueOnHeatMap = 0;

    this.draw = function(dimension, valueRange, data) {
        var rectSize = (this.height/this.resolution-1);
        maxValueOnHeatMap = 0;
        var x = d3.scaleLinear().range([0, this.width]);

        var y = d3.scaleLinear().range([this.height, 0]);
        y.domain(valueRange);
        x.domain(valueRange);
        var heatMapData = new Uint8Array(scope.resolution*scope.resolution);

        data.forEach(function (value) {
            var birth = Math.trunc(value.birth/valueRange[1]*(scope.resolution-1));
            var death = Math.trunc(value.death/valueRange[1]*(scope.resolution-1));
            heatMapData[birth+scope.resolution*death ] = heatMapData[birth+scope.resolution*death ]+1;
            if(heatMapData[birth+scope.resolution*death ] > maxValueOnHeatMap) maxValueOnHeatMap = heatMapData[birth+scope.resolution*death ];
        });


        console.log(heatMapData);

        var svg = d3.select(this.domElement).append("svg")
            .attr("width", this.width + this.margin.top + this.margin.bottom)
            .attr("height", this.height + this.margin.top + this.margin.bottom);

        var g = svg.append("g")
            .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");



        g.selectAll("rect")
            .data(heatMapData)
            .enter()
            .append("rect")
            .attr("width",rectSize)
            .attr("height",rectSize)
            .attr("transform",function(d,i) {
                var birth = i % scope.resolution;
                var death = (i-birth)/scope.resolution+1;
                //console.log(birth,death);
                return "translate(" +  (rectSize+1)*birth + ", " + (scope.height-(rectSize+1)*death) + ")"
            })
            .attr("fill",function (d,i) {
                return d === 0 ? "white" : scope.colorScheme(d/maxValueOnHeatMap)
            })
            .append("title").html(function (d,i) {
            var birth = i % scope.resolution;
            var death = (i-birth)/scope.resolution;
                return "birth: " + birth/scope.resolution*valueRange[1] + ", death: " + death/scope.resolution*valueRange[1] + ", amount: " + d
            });

        // Add the X Axis
        g.append("g")
            .attr("transform", "translate(0," + (this.height) + ")")
            .call(d3.axisBottom(x));

        // Add the Y Axis
        g.append("g")
            .call(d3.axisLeft(y));

    }
}