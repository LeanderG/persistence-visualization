import * as d3 from "d3";
import {getKey,chop,formatRange} from "./Util";

export function PersistenceDiagram() {

    // private
    var scope = this;
    //var data;
    var x;
    var y;
    var svg;
    var g;
    //var dimension;

    // public
    this.colorScheme = d3.schemeCategory10;

    this.margin = {top: 40, right: 20, bottom: 40, left: 60};
    this.width = 350;
    this.height = 350;
    this.opacity = 0.6;
    this.diameter = 12.0; // should be divisible by 2

    this.headingPerDimension = function (dimension) {
        return '<h4>Persistence intervals in dimension ' + dimension + '</h4>';
    };

    this.headingRange = function (range) {
        var formattedRange = formatRange(range);
        return '<h4>Distances have the range [ ' + formattedRange[0] + ", " + formattedRange[1] + ' ]</h4>'
    };

    this.outputRange = function () {
        d3.select(this.domElement).append("div").html(this.headingRange(this.distancesRange));
    };

    this.draw = function(dimension, valueRange, data) {
        x = d3.scaleLinear().range([0, this.width - this.margin.left - this.margin.right]);

        y = d3.scaleLinear().range([this.height - this.margin.top - this.margin.bottom, 0]);
        y.domain(valueRange);
        x.domain(valueRange);

        //insert heading
        d3.select(this.domElement).append("div").html(this.headingPerDimension(dimension));

        svg = d3.select(this.domElement).append("svg")
            .attr("width", this.width)
            .attr("height", this.height);
        g = svg.append("g")
            .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

        // Add the X Axis
        g.append("g")
            .attr("transform", "translate(0," + (this.height - this.margin.top - this.margin.bottom) + ")")
            .call(d3.axisBottom(x));

        // Add the Y Axis
        g.append("g")
            .call(d3.axisLeft(y));

        // Add dotted line
        g.append("line")
            .attr("x1", 0)
            .attr("y1", this.height - this.margin.top - this.margin.bottom)
            .attr("x2", this.width - this.margin.left - this.margin.right)
            .attr("y2", 0)
            .attr("stroke-widht", 2)
            .attr("stroke", "black")
            .attr("stroke-dasharray", "5,5");

        var dataDeadAfterFinalFiltration = data.filter(function (t) {
            return !t.alive;
        });
        var dataAliveAfterFinalFiltration = data.filter(function (t) {
            return t.alive;
        });
        g.selectAll("circle")
            .data(dataDeadAfterFinalFiltration)
            .enter()
            .append("circle")
            .attr("cx", function (d) {
                return x(d.birth);
            })
            .attr("cy", function (d) {
                return y(d.death);
            })
            .attr("r", this.diameter / 2)
            .attr("fill", this.colorScheme[dimension % 10])
            .attr("fill-opacity", this.opacity)
            .append("title").html(function (d) {
            return "[" + chop(d.birth).toString() + ",&thinsp;" + chop(d.death).toString() + ")";
        });


        g.selectAll("rect")
            .data(dataAliveAfterFinalFiltration)
            .enter()
            .append("rect")
            .attr("x", function (d) {
                return x(d.birth) - scope.diameter / 2;
            })
            .attr("y", function (d) {
                return y(d.death) - scope.diameter / 2;
            })
            .attr("width", this.diameter)
            .attr("height", this.diameter)
            .attr("fill", this.colorScheme[dimension % 10])
            .attr("fill-opacity", this.opacity)
            .append("title").html(function (d) {
            return "[" + chop(d.birth).toString() + ",&thinsp; )";
        });
    }

}