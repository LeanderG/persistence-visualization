


export function getKey(d) {
    return "[" + d.birth + "," + d.death + ")";
}

export function chop(x) {
    return typeof x === "number" ? x.toPrecision(6) / 1 : x;
}

export function formatRange(range) {

    var diff = range[1] - range[0];
    if (diff < 0.000000001)
        return range;
    var toFixed = Math.max(1, Math.log10(1 / diff) + 3);
    return [Number(range[0]).toFixed(toFixed), Number(range[1]).toFixed(toFixed)];
}

// https://stackoverflow.com/a/46427607
export function build_path(args) {
    return args.map(function(part, i){
        if (i === 0){
            return part.trim().replace(/[\/]*$/g, '')
        } else {
            return part.trim().replace(/(^[\/]*|[\/]*$)/g, '')
        }
    }).filter(function(x) {return x.length}).join('/')
}